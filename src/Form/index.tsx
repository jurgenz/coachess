import * as React from 'react';
import './form.scss';
import { MultiSelect } from './MultiSelect';

interface FormData {
  name: string;
  surname: string;
  languages: string[];
  rate: string;
  currency: string;
}

export const Form = () => {
  const [data, setData] = React.useState<FormData>({
    name: '',
    surname: '',
    languages: [],
    rate: '',
    currency: ''
  });

  const onFieldChange = (field: keyof FormData, value: string | string[]) => {
    setData({
      ...data,
      [field]: value
    });
  };

  const onSave = () => {
    alert(JSON.stringify(data));
  };

  return (
    <div className="form">
      <div className="input-wrap">
        <label>First name</label>
        <input value={data.name} onChange={e => onFieldChange('name', e.target.value)} />
      </div>

      <div className="input-wrap">
        <label>Last name</label>
        <input value={data.surname} onChange={e => onFieldChange('surname', e.target.value)} />
      </div>

      <MultiSelect label="Langauges you speak" onChange={value => onFieldChange('languages', value)} />

      <div className="input-wrap">
        <label>
          Hourly rate
          <span className="info">i</span>
        </label>
        <input className="small" value={data.rate} onChange={e => onFieldChange('rate', e.target.value)} />
        <select value={data.currency} onChange={e => onFieldChange('currency', e.target.value)}>
          <option>$ - US Dollar</option>
          <option>€ - Eur</option>
        </select>
      </div>

      <div className="input-wrap">
        <button onClick={onSave}>Save</button>
      </div>
    </div>
  );
};
