import * as React from 'react';
import { languages } from './languages';

type IMultiSelectProps = {
  label: string;
  onChange: (selected: string[]) => void;
};

export const MultiSelect = ({ label, onChange }: IMultiSelectProps) => {
  const [selected, setSelected] = React.useState<string[]>([]);
  const select = React.useRef<HTMLSelectElement>(null);

  const onAdd = () => {
    if (select.current) {
      const currentSelected = select.current.value;

      if (currentSelected) {
        const newSelected = [...selected, currentSelected];
        setSelected(newSelected);
        onChange(newSelected);
      }
    }
  };

  const onRemove = (index: number) => {
    const newSelected = [...selected.slice(0, index), ...selected.slice(index + 1)];

    setSelected(newSelected);
    onChange(newSelected);
  };

  const options = languages.filter(lang => selected.indexOf(lang) === -1);

  return (
    <div className="input-wrap multi-select">
      <label>{label}</label>
      <select ref={select} defaultValue="">
        <option disabled value="">
          Select language
        </option>
        {options.map(lang => (
          <option key={lang}>{lang}</option>
        ))}
      </select>
      <button onClick={onAdd}>Add</button>
      {selected.length ? (
        <div className="selected-options">
          {selected.map((lang, i) => (
            <div key={lang} className="item">
              <span className="language">{lang}</span>
              <span className="remove" onClick={() => onRemove(i)}>
                Remove
              </span>
            </div>
          ))}
        </div>
      ) : null}
    </div>
  );
};
