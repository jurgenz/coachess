import * as React from 'react';
import { steps } from './steps';
import { StepItem } from './StepItem';
import './steps.scss';

export const Steps = () => {
  const activeStep = 0;
  return (
    <div className="steps">
      {steps.map((step, index) => (
        <StepItem
          key={index}
          index={index + 1}
          title={step.title}
          isActive={index === activeStep}
          isLast={index === steps.length - 1}
        />
      ))}
    </div>
  );
};
