import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './main.scss';
import { Steps } from './Steps';
import { Form } from './Form';

const App = () => {
  return (
    <>
      <Steps />
      <Form />
    </>
  );
};

const node = document.getElementById('app');

ReactDOM.render(<App />, node);
