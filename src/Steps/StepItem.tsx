import * as React from 'react';

type IStepItemProps = {
  index: number;
  title: string;
  isActive: boolean;
  isLast: boolean;
};

export const StepItem = ({ index, title, isActive, isLast }: IStepItemProps) => {
  let className = 'steps-item';

  if (isActive) {
    className += ' active';
  }
  return (
    <div className={className}>
      <span className="count">{index}</span>
      <span className="title">{title}</span>
      {isLast ? null : <span className="chev">{'>'}</span>}
    </div>
  );
};
