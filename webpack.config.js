var path = require('path');
var BUILD_DIR = path.resolve(__dirname, './dist/js');
var APP_DIR = path.resolve(__dirname, './src/');

let production = true;
var config = {
  mode: 'development',
  entry: {
    main: APP_DIR + '/index.tsx'
  },
  output: {
    path: BUILD_DIR
  },
  devtool: '',
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json']
  },
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        include: APP_DIR,
        loader: 'ts-loader'
      },
      {
        exclude: /node_modules/,
        test: /\.js?/,
        include: APP_DIR,
        loader: 'babel-loader'
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Compiles Sass to CSS
          'sass-loader'
        ]
      }
    ]
  }
};

module.exports = config;
